###########################################################################################################
# 
# EECS 337-0
# Golden Globes Project
# February 17, 2015
#
# GROUP 10:   David Demeter
#             Rahul Matta
#             Sachin Lai
#             Michael Kushnit
#
# File:       golden21.py
#
###########################################################################################################

import sys
import json
import re
import string
import operator
from utils import *
from buildlists import *
import nltk

###########################################################################################################
#
# Functions used to convert unicode JSON to ASCII
#
###########################################################################################################

def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv

###########################################################################################################
#
# Main processing module for Golden Globes information extraction
#
# main(): extracts award categories, winners, presenters and nominees from Twitter JSOn for specified year
#
###########################################################################################################

def main(args):

    # process and verify command line inputs
    if len(args) != 4:
        print " "
        print "Error:  You need to specifiy 3 command line parameters."
        print "  usage: <inpput JSON file name> <output JSON file name> <award year>"
        debugPrompt()
        return

    inFileName = args[1]
    outFileName = args[2]
    year = int(args[3])

    if not (year == 2013 or year == 2015):
        print " "
        print "Error:  Year must be either 2013 or 2015."
        debugPrompt()
        return

    print " "
    print "Input File:     ",inFileName
    print "Output File:    ",outFileName
    print "Award Year:     ",year
    print " "
    print "Are these all correct?  Press 1 for Yes and 3 for No."
    debugPrompt()
        
    # initialized NLTK used for POS tagging
    tokenizer = nltk.data.load('tokenizers\punkt\english.pickle')

    # populate list of words to ignore and frames used to process tweets
    ignoreList = ['in', 'a', 'for', 'by', 'an', 'or']

    hostFrames = [['are hosting',' ', 5, 0]]
    
    winFrames = [['wins for',' ',2,0],
                 ['wins',' ',2,0],
                 ['wins then',' ',2,0],
                 ['congrats',' ',0,2],
                 ['goes to',' ',0,2]]

    nomFrames = [['nominated for',' ',2,0],
                 ['is nominated for',' ',2,0],
                 ['was nominated for',' ',2,0],
                 ['nominees are',' ',0,10],
                 ['nominees include',' ',0,10],
                 ['predict',' ',10,10]]
  
    presFrames = [['will present',' ',5,0],
                  ['will be presenting',' ',5,0],
                  ['introduced by', ' ', 0, 5],
                  ['presenters are', ' ', 0, 5],
                  ['is presenting', ' ', 2,0],
                  ['are presenting', ' ', 5, 0]]

    # set flags to toggle selected tasks on/off
    taskCat = 1
    taskWin = 1
    taskPres = 1
    taskNoms = 1
    taskHosts = 1
    taskFun = 1
    
    bVerbose = 0

    # create empty dictionaries to store retreived information
    dict0 = {}
    dictW = {}
    dictP1 = {}
    dictP2 = {}
    dictN2 = {}
    dictH = {}
    dictF = {}
    dictS = {}
    
    universe = []

    count = 0
    bDebug = False
    bIsActor = False

    # read specified JSON file
    print " "
    print "Reading JSON file",inFileName," ..."
    print " "

    jsonobj = json.load(open(inFileName,'r'),object_hook = _decode_dict)
    denom = len(jsonobj)
    
    print "Finished reading JSON file..."
    print " "

    # build nominee list based upon specified year
    nomList = buildNomineeList(year)

    print "Searching for potential award titles..."
    print " "
   
    # make first pass through tweets to identify potential award categories as consecutively
    # capitalized words following the word "Best"
    for tweet in jsonobj:
        line = tweet['text']

        count = count + 1
        if (count % (denom/10)) == 1:
            print "percent: ", floored_percentage(float(count)/float(denom),1)

        if line[0:2] != 'RT':

            if taskCat == 1:
              a = extractAward(line)
              if a:
                accumWords(dict0,"Best " + " ".join(a))
                universe.append(line)

    # select 150 most frequent potential award categories as working list
    # strip punctuation, certain stop word and sort in order of descending length
    if taskCat == 1:
      dict1 = sorted(dict0.items(), key = operator.itemgetter(1))
      if bDebug:
        for i in range(len(dict1)-150,len(dict1)):
          print dict1[i]

    awardList = list()
    for i in range(len(dict1)-150,len(dict1)):
      temp = dict1[i][0]
      temp = stripPunctuation(temp)
      temp = stripSubs(temp,['http'])
      if temp.find('Of') == -1 and temp.find('The') == -1:
        awardList.append(temp)

    awardList.sort(lambda x,y: cmp(len(y), len(x)))
    if bDebug:
      for i in range(0,len(awardList)):
        print awardList[i]
      debugPrompt()

    print " "
    print "Attempting to match possible winners with potential award titles..."
    print " "

    # Reprocess tweets which contained potential award categories.  Apply winFrames to identify
    # potential winners are capitalized words appearing immediately to the left or right of the
    # frame and compare against the list of nominees to validate the name as a potential winner

    # if a potential winner is found, it is added to a dictionary as a triple (award+winner, frequency)
    # for later consolidation and reduction
    count = 0
    for line in universe:

        count = count + 1
        if (count % (len(universe)/10)) == 1:
          print "percent: ", floored_percentage(float(count)/float(len(universe))+0.00005,1)

        if line[0:2] != 'RT':
            if taskWin == 1:
                award = ''
                aa = extractAward(line)
                for a in awardList:
                  if a == "Best " + " ".join(aa):
                    award = a
                    break
                if award:
                    for fr in winFrames:
                      [s,l,b,r] = applyFrame(line,fr[0],fr[1],fr[2],fr[3],' ')
                      if s != -1:
                        possibleName = " ".join(getAllCaps(l)) + " ".join(getAllCaps(r))
                        if bVerbose == 1:
                          print "Winner for", award, "|--> ", possibleName, "|-->  ", IsNominee(l,r,nomList)
                          print line

                        winner = IsNominee(l,r,nomList)

                        if winner:
                          addTripple(dictW,award,winner)


    # after processing all tweets determine which potential winner appears the most frequently
    # for each potential award category (which narrows the list of potential winners).  next
    # determine in which award categoy each winner appears and discard the rest (which narrows
    # the list of award categories.  these steps result in the final award and winner selections.
    if taskWin == 1:

      dict1 = sorted(dictW.items(), key = operator.itemgetter(1))
      if bDebug:
        print "RAW:"
        for i in range(1,len(dict1)):
          print dict1[i]
        debugPrompt()

      dict1 = cleanTripple(dict1)
 
      dict1 = sorted(dict1.items(), key = operator.itemgetter(1))
      if bDebug:
        print "FINAL:"
        for i in range(1,len(dict1)):
          print dict1[i]
        debugPrompt()

      [dict2,dictMap] = reduceTripple(dict1)
      if bDebug or 1:
        print " "
        print "System has identified the following winners and award categories..."
        print " "
        for i in range(0,len(dict2)):
          print "Award: ",dict2[i][0]," Winner: ",dict2[i][1]
        debugPrompt()

      listAW = dict2

      # create a set of frames for identified winner for sentiment processing below
      funFrames = []
      for i in range(0,len(dict2)):
        name = dict2[i][1]
#        frame = ['love ' + name,' ', 0,5]
        frame = [name,' ', 0,5]
        funFrames.append(frame)

      # create a dictionary of discarded award categories with selected award categories
      # to match potential presenters and nominees below
      dictM = sorted(dictMap.items(), key = operator.itemgetter(1))
      if bDebug:
        for i in range(0,len(dictM)):
          print dictM[i]
        debugPrompt()

    count = 0

    print " "
    print "Searcing for possible presenters, nominees, hosts and setiment data..."
    print "Note: This may take several minutes, please be patient.  Percent complete status"
    print "will be display for your enjoyment."
    print " "

    # process tweets one last time to extract potential presenters, nominees, hosts and "fun" facts
    # about each winner
    for tweet in jsonobj:
        line = tweet['text']

        count = count + 1
        if (count % (denom/10)) == 1:
          print "percent: ", floored_percentage(float(count)/float(denom),1)

        if line[0:2] != 'RT':

          # search for potential presenters by applying the frames specified above.  if a possible
          # presenter has been identified, validate that the entity is a personality by accessing
          # IMDB using the isActor() function -- see utils.py.  if the entity appears in IMDB
          # save the presenter name and award category as a triple for later processing of the type
          # used to identify awards and winners -- see above.
          if taskPres == 1:
            for fr in presFrames:
              [s,l,b,r] = applyFrame(line,fr[0],fr[1],fr[2],fr[3],' ')
              if s != -1:
                if bVerbose == 1:
                  print "Presenters: ", " ".join(l), " + ", " ".join(b), " + ", " ".join(r)

                L = getAllCaps(l)
                R = getAllCaps(r)
                isPresenter = 0
                if len(L) == 2:
                  possible = " ".join(L)
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)
                    
                if len(L) == 4:
                  possible = " ".join(L[:2])
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)
                  possible = " ".join(L[2:])
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)

                if len(R) == 2:
                  possible = " ".join(R)
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)
                    
                if len(R) == 4:
                  possible = " ".join(R[:2])
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)
                  possible = " ".join(R[2:])
                  if isActor(possible) and not possible.isupper():
                    isPresenter = 1
                    accumWords(dictP1,possible)

                if isPresenter == 1:
                  sometext = line
                  max_overlap = 0
                  best_award = ''
                  alist = getAwardKeys(dictM)
                  for a in alist:
                    this_overlap = overlap(a.lower().split(' '),sometext.lower().split(' '),['The','Of'])
                    if this_overlap > max_overlap:
                      max_overlap = this_overlap
                      best_award = a
                  if best_award:
                    a2 = returnAward(dictMap,best_award)
                    if a2:
                      addTripple(dictP2,dictMap[best_award],possible)

          # apply nomFrames to tweets to identify potential nominees.  access nomonees list to validate
          # potential nominees.  if validated, save nominee name and award as triple for later processing
          # of the type used to identify awards and winners -- see above
          if taskNoms == 1:
            for fr in nomFrames:
              [s,l,b,r] = applyFrame(line,fr[0],fr[1],fr[2],fr[3],' ')
              if s != -1:
                if bVerbose == 1:
                  print "Nominees: ", " ".join(l), " + ", " ".join(b), " + ", " ".join(r)

                nominee = IsNominee(l,r,nomList)

                if nominee:
                  aa = extractAward(line)
                  if aa:
                    for a in awardList:
                      if a == "Best " + " ".join(aa):
                        award = a
                        break
                    if award:
                      addTripple(dictN2,award,nominee)

          # apply hostFrame to tweets to identify potentis hosts.  hosts are identified as two most
          # frequent names derived from hostFrames.
          if taskHosts == 1:
            for fr in hostFrames:
              [s,l,b,r] = applyFrame(line,fr[0],fr[1],fr[2],fr[3],' ')
              if s != -1:
                L = getAllCaps(l)

                if len(L) == 4:
                  possible = " ".join(L[:2])
                  if isActor(possible) and not possible.isupper():
                    accumWords(dictH,possible)
                  possible = " ".join(L[2:])
                  if isActor(possible) and not possible.isupper():
                    accumWords(dictH,possible)

          if taskFun == 1:
            # apply frame to tweets to identify potential sponsors, taken to be the single
            # capitalized word following the frame
            [s,l,b,r] = applyFrame(line,'sponsored by',' ',0,1,' ')
            if s != -1:
              R = getAllCaps(r)
              if R:
                sponsor = " ".join(R)
                if not re.search("golden",sponsor):
                  accumWords(dictS,sponsor)

            # apply each of the "loves <winner's name>" frame created above to collect potential
            # sentiment information about each winner.  used the NLTK to conduct POS tagging on
            # the phrase following the frame and select the closed adjective and a proxy for the
            # sentiment about the winner, with the most frequent adjective being selected
            for fr in funFrames:
              [s,l,b,r] = applyFrame(line,fr[0],fr[1],fr[2],fr[3],' ')
              if s != -1:
                if len(r) > 1:
                  test = "".join(r)
                  if test.isalnum():
                      pos = nltk.pos_tag(r)
                      for i in range(0,len(pos)):
                        if pos[i][1] == 'JJ':
                          tt = fr[0].split(' ')
                          winner = " ".join(tt[1:])
                          adjective = pos[i][0]
                          if len(adjective) > 0 and adjective.lower() != 'much' and adjective.lower() != 'dont':
                            addTripple(dictF,winner,adjective)
                      


    # process each of the potential presenters and potential nominees triples to select the most frequent
    # by award category.  select the most frequent adjective as the sentiment about each winner, and the most
    # frequent two potential hosts and the hosts.
    if taskPres == 1:      
      dict1 = sorted(dictP1.items(), key = operator.itemgetter(1))
      if bDebug:
        for i in range(1,len(dict1)):
          print dict1[i]
        debugPrompt()

      listP = dict1

      dict1 = sorted(dictP2.items(), key = operator.itemgetter(0))
      presenterPairs = getPresenterPairs(dict1)
      if bDebug or 1:
        print " "
        print "The following award-presenter pairs have been identified..."
        print "Note: Presenters identified by not pair with an award will be storted in the"
        print "unstructured JSON, but will not be displayed here."
        print " "
        for i in range(1,len(presenterPairs)):
          print "Award: ",presenterPairs[i][0]," Presenter: ",presenterPairs[i][1]
        debugPrompt()

      listAP = presenterPairs
      
    if taskNoms == 1:      
      dict1 = sorted(dictN2.items(), key = operator.itemgetter(0))
      nomineePairs = getPresenterPairs(dict1)
      if bDebug or 1:
        print " "
        print "The following award-nominee pairs have been identified..."
        print " "
        for i in range(1,len(nomineePairs)):
          print "Award: ",nomineePairs[i][0]," Nominee: ",nomineePairs[i][1]
        debugPrompt()


      dict1 = sorted(dictMap.items(), key = operator.itemgetter(0))
      if bDebug:
        for i in range(0,len(dict1)):
          print dict1[i]

      listN = nomList
      listAN = list()
      for i in range(0,len(nomineePairs)):
        try:
          listAN.append([dictMap[nomineePairs[i][0]],nomineePairs[i][1]])
        except KeyError:
          if bDebug:
            print "revocered?"

    if taskFun == 1:
      dict1 = sorted(dictF.items(), key = operator.itemgetter(1))
      if bDebug:
        for i in range(1,len(dict1)):
          print dict1[i]
        debugPrompt()

      dict1 = cleanTripple(dict1)
 
      dict1 = sorted(dict1.items(), key = operator.itemgetter(1))
      if bDebug or 1:
        print " "
        print "Also, it may interest you to know that:"
        print " "
        for i in range(1,len(dict1)):
          print "  ",dict1[i][0],' is described as ',dict1[i][1][0]
        debugPrompt()

      listS = dict1

    if taskHosts == 1:      
      dict1 = sorted(dictH.items(), key = operator.itemgetter(1))
      host1 = dict1[len(dict1)-1][0]
      host2 = dict1[len(dict1)-2][0]
      if bDebug:
        for i in range(1,len(dict1)):
          print dict1[i]
        debugPrompt()
      print " "
      print "... and finally, tonights Hosts were: ", host1, " and ", host2, "!!!"
      print " "


    if taskFun == 1:
      dict1 = sorted(dictS.items(), key = operator.itemgetter(1))
      if bDebug or 1:
        print " "
        print "We would like to thank our wonderful sponsors: ",
        for i in range(1,len(dict1)-1):
          print dict1[i][0],", ",
        print "and ",dict1[len(dict1)-1][0],"."
        debugPrompt()

      listSP = dict1


    print " "
    print "Please wait while we store our results in a JSON file..."
    print " "

    # append text to each award category not otherwise identifed as an award relating to TV or a miniseries
    for i in range(0,len(listAW)):
        award = listAW[i][0]
        winner = listAW[i][1]
        listAN.append([award,winner])
        if (re.search("series",award,re.IGNORECASE) and not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE))):
            award = award.replace("Series","Television Series")
        if not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE) or re.search("mini",award,re.IGNORECASE)):
            award = award + " in a Motion Picture"
        award = award.replace("TV","Television")
        award = award.replace("tv","Television")
        listAW[i][0] = award
        
    if taskPres == 1:
        for i in range(0,len(listAP)):
            award = listAP[i][0]
            winner = listAP[i][1]
            if (re.search("series",award,re.IGNORECASE) and not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE))):
                award = award.replace("Series","Television Series")
            if not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE) or re.search("mini",award,re.IGNORECASE)):
              award = award + " in a Motion Picture"
            award = award.replace("TV","Television")
            award = award.replace("tv","Television")
            listAP[i][0] = award

    if taskNoms == 1:
        for i in range(0,len(listAN)):
            award = listAN[i][0]
            winner = listAN[i][1]
            if (re.search("series",award,re.IGNORECASE) and not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE))):
                award = award.replace("Series","Television Series")
            if not (re.search("tv",award,re.IGNORECASE) or re.search("television",award,re.IGNORECASE) or re.search("mini",award,re.IGNORECASE)):
                award = award + " in a Motion Picture"
            award = award.replace("TV","Television")
            award = award.replace("tv","Television")
            listAN[i][0] = award


    # pad the award list with dummy values if too few awards were identified.  this is to avoid key errors
    # in the output JSON object
    for i in range(len(listAW),25):
        listAW.append(['Award'+str(i),'Winner'+str(i)])

    # create the skeleton of the JSON object specified to report results
    jsonobj = {
                "metadata":
                  {
                    "year": year,
                    "names":
                        {
                        "hosts": {"method": "detected", "method_description": "none" },
                        "nominees": {"method": "hardcoded", "method_description": "Nominees for unstructured data was hard-coded, and detected for association with awards." },
                        "awards": {"method": "detected", "method_description": "none" },
                        "presenters": {"method": "detected", "method_description": "none" }
                        },
                    "mappings":
                        {
                        "nominees": {"method": "detected", "method_description": "none"},
                        "presenters": {"method": "detected", "method_description": "none"}
                        }
                  },
                "data":
                  {
                    "unstructured": {"hosts": [], "winners": [], "awards": [], "presenters": [], "nominees": []},
                    "structured":
                      {
                        listAW[0][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[1][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[2][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[3][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[4][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[5][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[6][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[7][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[8][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[9][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[10][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[11][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[12][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[13][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[14][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[15][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[16][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[17][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[18][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[19][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[20][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[21][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[22][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[23][0]: {"nominees": [],"winner": '',"presenters": []},
                        listAW[24][0]: {"nominees": [],"winner": '',"presenters": []}
                      }
                  }
              }

    # populate the JSON skeleton with the hosts, awards, winners, presenters and nominees
    # for both unstructured and structured data.
    if taskHosts == 1:
      jsonobj["data"]["unstructured"]["hosts"].append(host1)
      jsonobj["data"]["unstructured"]["hosts"].append(host2)

    for i in range(0,min(len(listAW),25)):
      award = listAW[i][0]
      winner = listAW[i][1]
      jsonobj["data"]["unstructured"]["winners"].append(winner)
      jsonobj["data"]["unstructured"]["awards"].append(award)
      try:
        jsonobj["data"]["structured"][award]["winner"] = winner
      except KeyError:
        if bDebug:
          print "recoveredA?"

    if taskPres == 1:
      for i in range(0,len(listP)):
        presenter = listP[i][0]
        jsonobj["data"]["unstructured"]["presenters"].append(presenter)      

      for i in range(0,len(listAP)):
        award = listAP[i][0]
        presenter = listAP[i][1]
        try:
            jsonobj["data"]["structured"][award]["presenters"].append(presenter)
        except KeyError:
            print "recoveredB?"

    if taskNoms == 1:
      for i in range(0,len(listN)):
        nominee = listN[i]
        jsonobj["data"]["unstructured"]["nominees"].append(nominee)      

      for i in range(0,len(listAN)):
        award = listAN[i][0]
        nominee = listAN[i][1]
        try:
            jsonobj["data"]["structured"][award]["nominees"].append(nominee)
        except KeyError:
            print "recoveredC?"

    # save the JSON data to the file specified on the command line.
    with open(outFileName,'w') as outFile:
      json.dump(jsonobj,outFile,indent=4)
    outFile.close()

    # create, populate and save a JSON object to store the information retreived for "Fun" goals
    # that will be used by the GUI
    jsonobj = {
                "sponsors": [],
                "sentiment":
                  {
                    listAW[0][1]: '',
                    listAW[1][1]: '',
                    listAW[2][1]: '',
                    listAW[3][1]: '',
                    listAW[4][1]: '',
                    listAW[5][1]: '',
                    listAW[6][1]: '',
                    listAW[7][1]: '',
                    listAW[8][1]: '',
                    listAW[9][1]: '',
                    listAW[10][1]: '',
                    listAW[11][1]: '',
                    listAW[12][1]: '',
                    listAW[13][1]: '',
                    listAW[14][1]: '',
                    listAW[15][1]: '',
                    listAW[16][1]: '',
                    listAW[17][1]: '',
                    listAW[18][1]: '',
                    listAW[19][1]: '',
                    listAW[20][1]: '',
                    listAW[21][1]: '',
                    listAW[22][1]: '',
                    listAW[23][1]: '',
                    listAW[24][1]: '',
                  }
              }

    if taskFun == 1:
      for i in range(0,len(listSP)):
        sponsor = listSP[i][0]
        jsonobj["sponsors"].append(sponsor)
        
      for i in range(0,len(listS)):
        winner = listS[i][0]
        adjective = listS[i][1][0]
        jsonobj["sentiment"][winner] = adjective

    outFileName = 'fun_' + outFileName
    with open(outFileName,'w') as outFile:
      json.dump(jsonobj,outFile,indent=4)
    outFile.close()

    print " "
    print "* * * * * * * * * * D O N E * * * * * * * * * *"
    print " "
    print "* * * * * * * T H A N K   Y O U * * * * * * * *"
    print " "

if __name__ == "__main__":
    main(sys.argv)
