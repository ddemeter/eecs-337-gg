###########################################################################################################
# 
# EECS 337-0
# Golden Globes Project
# February 17, 2015
#
# GROUP 10:   David Demeter
#             Rahul Matta
#             Sachin Lai
#             Michael Kushnit
#
# File:       utils.py
#
###########################################################################################################



from bs4 import BeautifulSoup
import urllib2

import sys
import json
import re
import string
import operator
from math import floor


###########################################################################################################
#
# Function which validates a potential name using IMDB
#
# isActor(): validates an actors name bu checking www.imdb.com and checking for an image
#
###########################################################################################################

def isActor(actor):
  url = "http://www.imdb.com/find?q=" + actor.replace(" ","%20") + "&&s=nm&&exact=true"
  try:
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    raw = soup.find_all('table', attrs={'class':'findList'})[0].find('a')
    img = raw.find('img')
    if (raw and img['src'] != "http://ia.media-imdb.com/images/G/01/imdb/images/nopicture/32x44/name-2138558783._CB379389446_.png"):
      return True
    else:
      return False
  except:
    return False

###########################################################################################################
#
# Function to extract a given number of words to the left of, inbetween and to the right of a frame
#
# applyFrame(): applies frame to a tweet to extract left, inbetween and right words
#
###########################################################################################################

def applyFrame(rawTweet, leftPhrase, rightPhrase, numBefore, numAfter, ignoreWords):
  leftWords = ['']
  betweenWords = ['']
  rightWords = ['']

#  if rawTweet[0:2] == 'RT' and 0:
#    return [-1,leftWords,betweenWords,rightWords]

  rawTweet = stripPunctuation(rawTweet)

  text = rawTweet.lower()
  left = leftPhrase.lower()
  right = rightPhrase.lower()
  
  tokens = text.split(' ')
  bigTokens = rawTweet.split(' ')

  [startLeft,endLeft] = getPhraseIndices(tokens,left,-1)

  if startLeft == -1:
    return [-1,leftWords, betweenWords, rightWords]

  length = len(tokens)
  a = max(0, startLeft-numBefore)
  b = min(len(tokens),endLeft+numAfter)

  leftWords = bigTokens[a:startLeft]
  rightWords = bigTokens[endLeft:b]

  if rightPhrase != ' ':
    [startRight,endRight] = getPhraseIndices(tokens,right,-1)
    if startRight != -1 and endRight != -1:
      b = min(len(tokens),endRight+numAfter)
      rightWords = bigTokens[endRight:b]
      betweenWords = bigTokens[endLeft:endRight]
      
  return [1,leftWords, betweenWords, rightWords]


###########################################################################################################
#
# Functions to handle the identification and extraction of potential award titles
#
# extractAward(): strips extraneous information from potential award string
# getAwardWords(): searches tweet for all capitalized words following the word "Best"
#
###########################################################################################################

def getAwardWords(text):
  award = list()
  okayWords = ['in','a','or','-']
  stopPunc = [':','.','The','At','Goldes','Globes']
  dashCount = 0
  
  tokens = text.split(' ')
  i = getTokenIndex(tokens,"Best",-1)
  if i == -1:
    return award
  
  for j in range(i+1,len(tokens)):
    s = tokens[j]
    if s:
      if s == '-':
        dashCount = dashCount + 1
        if dashCount == 2:
          return award
      if s[0].isupper() or s in okayWords:
        award.append(s)
      else:
        return award
      for p in stopPunc:
        if s.find(p) != -1:
          return award
  return award

def extractAward(text):
#  if text[0:2] == 'RT' and 0:
#    return ''

  tokens = getAwardWords(text)
  if len(tokens) == 0:
    return ''

  words = " ".join(tokens)
  words = stripPunctuation(words)
  words = stripIgnores(words,['At','Golden','Globes','Or','in','a','or','  '])

  return words.split(' ')





###########################################################################################################
#
# Functions to handle the building, consolidation and extraction of triples using dictionaries
#
# addTripple(): encode an award+winner or award+nominee, etc. into a dict and tracks frequency
# cleanTripple(): unpacks compound key and reduces to potential winners to one per award category
# reduceTripple(): reduces potential award categories to ones with most frequent winner
#
###########################################################################################################

def addTripple(dict0,award,name):
  key = award + "+" + name
  try:
    val = dict0[key]
  except KeyError:
    val = 0
  val = val + 1
  dict0[key] = val

def cleanTripple(dict0):
  dict1 = {}
  i = 0
  while len(dict0) > 0:
    record1 = dict0[0]
    del dict0[0]
    m = record1[0].find('+')
    award1 = record1[0][0:m]
    name1 = record1[0][m+1:]
    best = record1[1]
    name = name1;
    
    j = 0
    while j < len(dict0):
      record2 = dict0[j]
      n = record2[0].find('+')
      award2 = record2[0][0:n]
      name2 = record2[0][n+1:]
      if award1 == award2:
        del dict0[j]
        if record2[1] > best:
          best = record2[1]
          name = name2
      j = j + 1
    dict1[award1] = [name,best]
    
  return dict1

def reduceTripple(dict0):
  l = []
  dictMap = {}
  listMap = []
  for i in range(0,len(dict0)-1):
    entry1 = dict0[i]
    entry2 = dict0[i+1]
    
    listMap.append(entry2[0])
    
    if entry1[1][0] != entry2[1][0]:
      award = entry1[0]
      name = entry1[1][0]
      num = entry1[1][1]
      if num > 3:
        l.append([award,name,num])
        dictMap[award] = award
        for l2 in listMap:
          dictMap[l2] = award
        listMap = []

  award = entry2[0]
  name = entry2[1][0]
  num = entry2[1][1]
  if num > 3:
    l.append([award,name,num])
  return [l,dictMap]



###########################################################################################################
#
# Functions used to retrieve capitalized and consecutively capitalized words.
#
# getAllCaps(): gets all words in a string with initial caps
# getLeftCaps(): gets up to N words to the left of an index with initial caps
# getRightCaps(): gets up to N words to the right of an index with initial caps
#
###########################################################################################################

# functions to retreive all, or consecutively capitalized words
def getAllCaps(tokens):
  returnList = list()
  for t in tokens:
    str = " ".join(t)
    if len(str) > 0:
      if str[0].isupper():
        returnList.append(t)
  return returnList

def getLeftCaps(tokens,start):
  returnList = list()
  i = start
  while 1:
    i = i - 1
    if i < 0:
      return returnList
    str = tokens[i]
    if len(str) > 0:
      if str[0].isupper():
        returnList.insert(0,tokens[i])
      else:
        return returnList
  return returnList

def getRightCaps(tokens,start,ignorables):
  returnList = list()
  i = start
  while 1:
    i = i + 1
    if i > len(tokens)-1:
      return returnList
    str1 = tokens[i]
    if len(str1) > 0:
      if str1[0].isupper() or str1[len(str1)-1] == ":":
        returnList.append(tokens[i])
      else:
        if str1 in ignorables:
          continue
        else:
          return returnList
  return returnList


###########################################################################################################
#
# Functions to stip unwanted punctuation, stop words or words containing specified substrings from tokens
#
# stripPunctuation(): strips all punctuation for a string
# stripIgnores(): strips all "ignore" words from a string
# stripSubs(): strips all words containing a sub-string from a string
#
###########################################################################################################

def stripPunctuation(inText):
  table = string.maketrans("","")
  outText = inText.translate(table, string.punctuation)
  return outText

def stripIgnores(inText,ignoreWords):
  tokens = inText.split(' ')
  for ig in ignoreWords:
    i = getTokenIndex(tokens,ig,-1)
    if i != -1:
      tokens.remove(ig)
  outText = " ".join(tokens)
  return outText

def stripSubs(inTokens,subList):
  outTokens = inTokens
  for s in subList:
    for t in outTokens:
      if re.search(s,t,re.IGNORECASE):
        outTokens.remove(t)
  return outTokens


###########################################################################################################
#
# Functions used to encapsulate text processing and dictionary management
#
# overlap(): computers the word overlap for two sets of tokens
# accumWords(): adds a phrase to a dictionary and maintains information about its frequency
# IsNominee(): validates a potential nominee by searching the nominee list
# getTokenIndex(): retrieves the index for an element is a set of tokens
# getPhraseIndices(): retreives the start and end indices for a phrase is a set of tokens
# getPresenterPairs(): unpacks an award+name pair to return a list of tupples
#
###########################################################################################################

def overlap(tok1, tok2, ignorables):
  count = 0
  for tk in tok1:
    if not (tk in ignorables):
      if getTokenIndex(tok2,tk,-1) != -1:
        count = count + 1
  return count

def accumWords(dict0,token):
  try:
    val = dict0[token]
  except KeyError:
    val = 0
  val = val + 1
  dict0[token] = val

def IsNominee(l,r,nomList):
  lC = getAllCaps(l)
  rC = getAllCaps(r)
  lC = stripSubs(lC,['The','Of'])
  rC = stripSubs(rC,['The','Of'])
  for name in nomList:
    for t in lC:
      t = stripPunctuation(t)
      if re.search(t,name):
        return name
    for t in rC:
      t = stripPunctuation(t)
      if re.search(t,name):
        return name
  return ''

def getTokenIndex(self, elem, fail):
    try:
        index = self.index(elem)
    except ValueError:
        return fail
    return index

def getPhraseIndices(tokens, phrase, fail):
  phraseTokens = phrase.split(' ')
  start = -1
  end = -1

  i = 0
  for p in phraseTokens:
    cur = getTokenIndex(tokens,p,-1)
    if cur == -1:
      return [fail,fail]
    if start == -1:
      start = cur
    if cur - start != i:
      return [fail,fail]
    end = cur + 1
    i = i + 1
  return[start, end]

def getPresenterPairs(alist):
  blist = list()
  for i in range(0,len(alist)):
    entry = alist[i]
    m = entry[0].find('+')
    if m != -1:
      award = entry[0][:m]
      name = entry[0][m+1:]
      blist.append([award,name])
  return blist

def returnAward(dict0,key):
  try:
    return dict0[key]
  except KeyError:
    return ''

def getAwardKeys(dict0):
  alist = []

  for i in range(0,len(dict0)):
    entry = dict0[i]
    award = entry[0]
    alist.append(award)
  return alist



###########################################################################################################
#
# Functions used faciliate status updates while processing the tweets
#
# debugPrompt(): paused execution and provides the user with an opportunity to exit
# floored_percentage(): formats percent complete for screen output
#
###########################################################################################################


#used to pause execution and provide an opportumity to exit
def debugPrompt():
    print " "
    xxx = input('ENTER \'3\' to STOP: ')
    if xxx == 3:
        exit(0)

def floored_percentage(val, digits):
  val *= 10 ** (digits + 2)
  return '{1:.{0}f}%'.format(digits, floor(val) / 10 ** digits)


